# openssl2iana
Convert either OpenSSL ciphers names or Values like "0xC0,0x12" to registered IANA names.

Eventually, get the updated [IANA reference](http://www.iana.org/assignments/tls-parameters/tls-parameters-4.csv)

# Usage

Pipe names into the script: `cat examples/mozilla-modern.txt | cut -d' ' -f5 | ./resolve.sh`

- Instead of OpenSSL names, you could also use the Values "0x00...": `cat examples/mozilla-modern.txt | cut -d' ' -f1 | ./resolve.sh` 
- You can also use it directly: `./openssl2iana.sh AES128-SHA256` or  `./openssl2iana.sh "0xC0,0x14"`
- Also: `openssl ciphers -V 'EDH+aRSA+AES256:EECDH+aRSA+AES256' | cut -d'-' -f1 | ./resolve.sh`

# Gotchas

It relies on *your* installed version of OpenSSL. Depending on the version and compil option, you may not have all the conversions available. `./openssl2iana.sh GOST94-GOST89-GOST89` : NO match !

OpenSSL is a bit ahead of IANA. It implements cipher-suites that are not registred yet. `./openssl2iana.sh ECDHE-ECDSA-CHACHA20-POLY1305` : NO match !


*If someone has an idea on how to solve this issue, send a mail, thanks.*

# Misc 

Error and non-matching names/values reporting could be improved! (see Gotchas)

Inspired from  http://stackoverflow.com/questions/19846020/how-to-map-a-openssls-cipher-list-to-java-jsse

Useful to convert cipher-suites lists from: 
 - https://wiki.mozilla.org/Security/Server_Side_TLS 
 - https://github.com/ssllabs/research/wiki/SSL-and-TLS-Deployment-Best-Practices
 - ...

The `openssl ciphers -stdname` option can precede each ciphersuite by its standard name. *But* only if OpenSSL has been built with tracing: `enable-ssl-trace` [OpenSSL ciphers](https://www.openssl.org/docs/manmaster/apps/ciphers.html).

#!/bin/sh

# Accept either the code: 0x
#  or the OpenSSL name
#  And convert it to IANA name

CIPHER=${1:-"CRAP"}

if [[ "$CIPHER" = 0x* ]]; then 
    CODE="$CIPHER"
else
    # Spaces arround CIPHER are important to match a single name
    CODE=$(openssl ciphers -V ALL | grep " $CIPHER " | sed 's/ //g' | cut -d'-' -f1 )
fi

if [ -z "$CODE" ]; then
    echo "NO match !"
    exit 1
fi
grep "$CODE" tls-parameters-4.csv | cut -d',' -f3

